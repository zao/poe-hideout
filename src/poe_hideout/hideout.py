from typing import Union
from . import poe_hideout_bindings as phb
import pathlib

def load_hideout(path: Union[str, pathlib.Path]):
    path = pathlib.Path(path)
    with path.open(encoding='utf-8-sig') as fh:
        return phb.open_hideout(fh.read())

def save_hideout(hideout, path: Union[str, pathlib.Path]):
    path = pathlib.Path(path)
    with path.open('w', encoding='utf-8-sig') as fh:
        fh.write(phb.export_hideout_json_format(hideout))