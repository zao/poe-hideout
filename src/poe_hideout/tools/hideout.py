import click
import sys
from poe_hideout import load_hideout, save_hideout

@click.group()
def cli():
    pass

@click.command('shift')
@click.argument('input_hideout', type=click.Path(exists=True))
@click.argument('output_hideout', type=click.Path())
@click.option('--x', default=0, help='Amount to offset on the X axis')
@click.option('--y', default=0, help='Amount to offset on the Y axis')
def hideout_shift_command(input_hideout, output_hideout, x, y):
    ho = load_hideout(input_hideout)
    for obj in ho.objects:
        if obj.x + x < 0 or obj.y + y < 0:
            raise RuntimeError("Cowardly refusing to move decoration into negative coordinate")
        obj.x += x
        obj.y += y
    save_hideout(ho, output_hideout)

cli.add_command(hideout_shift_command)