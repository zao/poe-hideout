#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "poe_hideout_core.hpp"

#include <fmt/format.h>

namespace py = pybind11;
using namespace py::literals;

PYBIND11_MODULE(poe_hideout_bindings, m) {
    m.doc() = "Modern Path of Exile hideout loading/saving";

    m.def("open_hideout", poe_hideout::open_hideout, "contents"_a);
    m.def("export_hideout_json_format", poe_hideout::export_hideout_json_format, "hideout"_a);

    py::class_<poe_hideout::hideout_object>(m, "hideout_object")
        .def_readwrite("name", &poe_hideout::hideout_object::name_)
        .def_readwrite("hash", &poe_hideout::hideout_object::hash_)
        .def_readwrite("x", &poe_hideout::hideout_object::x_)
        .def_readwrite("y", &poe_hideout::hideout_object::y_)
        .def_readwrite("rot", &poe_hideout::hideout_object::rot_)
        .def_readwrite("flip", &poe_hideout::hideout_object::flip_)
        .def_readwrite("var", &poe_hideout::hideout_object::var_)
        .def("__repr__", [](poe_hideout::hideout_object const &obj) {
            return fmt::format("<poe_hideout::hideout_object {name} at {x},{y}, rot {rot}, flip {flip}, var {var}",
                               fmt::arg("name", obj.name_), fmt::arg("x", obj.x_), fmt::arg("y", obj.y_),
                               fmt::arg("rot", obj.rot_), fmt::arg("flip", obj.flip_), fmt::arg("var", obj.var_));
        });

    py::class_<poe_hideout::hideout, std::shared_ptr<poe_hideout::hideout>>(m, "hideout")
        .def_readwrite("language", &poe_hideout::hideout::language_)
        .def_readwrite("hideout_name", &poe_hideout::hideout::hideout_name_)
        .def_readwrite("hideout_hash", &poe_hideout::hideout::hideout_hash_)
        .def_readwrite("music_name", &poe_hideout::hideout::music_name_)
        .def_readwrite("music_hash", &poe_hideout::hideout::music_hash_)
        .def_readwrite("objects", &poe_hideout::hideout::objects_)
        .def("__repr__", [](poe_hideout::hideout const &ho) {
            std::string first_obj = ho.objects_.empty() ? "Nada" : ho.objects_[0].name_;
            return fmt::format(
                "<poe_hideout::hideout of base {hideout_name}, {obj_count} objects, first object: {first_obj}",
                fmt::arg("hideout_name", ho.hideout_name_), fmt::arg("obj_count", ho.objects_.size()),
                fmt::arg("first_obj", first_obj));
        });

#ifdef VERSION_INFO
    m.attr("__version__") = VERSION_INFO;
#else
    m.attr("__version__") = "dev";
#endif
}