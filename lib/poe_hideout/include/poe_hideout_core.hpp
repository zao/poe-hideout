#pragma once

#include "nlohmann/json.hpp"

#include <fstream>
#include <optional>
#include <regex>
#include <span>
#include <sstream>
#include <variant>

namespace poe_hideout {
struct hideout_object {
    std::string name_;
    uint32_t hash_;
    uint32_t x_;
    uint32_t y_;
    uint16_t rot_;
    bool flip_;
    uint8_t var_;

    static std::optional<hideout_object> from_json(nlohmann::json js);
    nlohmann::json to_json() const;
};

struct hideout {
    std::string language_;
    std::string hideout_name_;
    uint16_t hideout_hash_;
    std::optional<std::string> music_name_;
    std::optional<uint16_t> music_hash_;
    std::optional<int> environment_;

    std::vector<hideout_object> objects_;
};

std::shared_ptr<hideout> open_hideout(std::string_view contents);

std::optional<std::string> export_hideout_json_format(hideout const &p);
} // namespace poe_hideout