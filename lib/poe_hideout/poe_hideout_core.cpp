#include "poe_hideout_core.hpp"

#include <fmt/format.h>

namespace poe_hideout {
struct HideoutSAX : nlohmann::json_sax<nlohmann::json> {
    bool null() override { return false; }
    bool boolean(bool) override { return false; }

    template <typename T> bool handle_number(T val) {
        if (path_.empty()) {
            return false;
        }
        auto back = path_.back();
        if (back == Thing::Version) {
            version = val;
        } else if (back == Thing::HideoutHash) {
            proj_.hideout_hash_ = (uint16_t)val;
        } else if (back == Thing::MusicHash) {
            proj_.music_hash_ = (uint16_t)val;
        } else if (back == Thing::Environment) {
            proj_.environment_ = (int)val;
        } else if (back == Thing::DoodadHash) {
            current_object_->hash_ = (uint32_t)val;
        } else if (back == Thing::DoodadX) {
            current_object_->x_ = (uint32_t)val;
        } else if (back == Thing::DoodadY) {
            current_object_->y_ = (uint32_t)val;
        } else if (back == Thing::DoodadR) {
            current_object_->rot_ = (uint16_t)val;
        } else if (back == Thing::DoodadFV) {
            current_object_->flip_ = !!(val & 0x80);
            current_object_->var_ = val & 0x7F;
        } else {
            return false;
        }
        path_.pop_back();
        return true;
    }

    bool number_integer(number_integer_t val) override { return handle_number(val); }

    bool number_unsigned(number_unsigned_t val) override { return handle_number(val); }

    bool number_float(number_float_t, string_t const &) override { return false; }

    bool string(string_t &val) override {
        if (path_.empty()) {
            return false;
        }
        auto back = path_.back();
        if (back == Thing::Language) {
            proj_.language_ = std::move(val);
        } else if (back == Thing::HideoutName) {
            proj_.hideout_name_ = std::move(val);
        } else if (back == Thing::MusicName) {
            proj_.music_name_ = std::move(val);
        } else if (back == Thing::Doodads) {
            current_object_->name_ = std::move(val);
        } else {
            return false;
        }
        path_.pop_back();
        return true;
    }

    bool binary(binary_t &val) override { return false; }

    bool start_object(size_t elements) override {
        if (path_.empty()) {
            return false;
        }
        auto const back = path_.back();
        if (back == Thing::Top) {
        } else if (back == Thing::Doodads) {
            current_object_ = hideout_object{};
        } else if (back == Thing::DoodadObject) {
        } else {
            return false;
        }
        return true;
    }

    bool end_object() override {
        if (path_.empty()) {
            return false;
        }
        auto const back = path_.back();
        if (back == Thing::DoodadObject) {
            path_.pop_back();
            objects_.push_back(*current_object_);
            current_object_.reset();
            current_object_ = hideout_object{};
            return true;
        } else if (back == Thing::Doodads) {
            path_.pop_back();
            proj_.objects_ = std::move(objects_);
            return true;
        } else if (back == Thing::Top) {
            path_.pop_back();
            return true;
        }
        return false;
    }

    bool start_array(size_t elements) override { return false; }
    bool end_array() override { return false; }

    bool key(string_t &key) override {
        if (path_.empty()) {
            return false;
        }
        auto const back = path_.back();
        if (back == Thing::Top) {
            if (key == "version") {
                path_.push_back(Thing::Version);
            } else if (key == "language") {
                path_.push_back(Thing::Language);
            } else if (key == "hideout_name") {
                path_.push_back(Thing::HideoutName);
            } else if (key == "hideout_hash") {
                path_.push_back(Thing::HideoutHash);
            } else if (key == "music_name") {
                path_.push_back(Thing::MusicName);
            } else if (key == "music_hash") {
                path_.push_back(Thing::MusicHash);
            } else if (key == "environment") {
                path_.push_back(Thing::Environment);
            } else if (key == "doodads") {
                path_.push_back(Thing::Doodads);
            } else {
                return false;
            }
        } else if (back == Thing::Doodads) {
            current_object_->name_ = std::move(key);
            path_.push_back(Thing::DoodadObject);
        } else if (back == Thing::DoodadObject) {
            if (key == "hash") {
                path_.push_back(Thing::DoodadHash);
            } else if (key == "x") {
                path_.push_back(Thing::DoodadX);
            } else if (key == "y") {
                path_.push_back(Thing::DoodadY);
            } else if (key == "r") {
                path_.push_back(Thing::DoodadR);
            } else if (key == "fv") {
                path_.push_back(Thing::DoodadFV);
            } else {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    bool parse_error(size_t, std::string const &, nlohmann::detail::exception const &) override { return false; }

    enum class ObjType {};

    enum class Thing {
        Top,
        Version,
        Language,
        HideoutName,
        HideoutHash,
        MusicName,
        MusicHash,
        Environment,
        Doodads,
        DoodadObject,
        DoodadHash,
        DoodadX,
        DoodadY,
        DoodadR,
        DoodadFV,
    };

    std::vector<Thing> path_{Thing::Top};
    std::string current_key_;
    std::optional<hideout_object> current_object_;

    size_t version{};
    hideout proj_{};
    std::vector<hideout_object> objects_;
};

std::shared_ptr<hideout> open_hideout(std::string_view contents) {
    HideoutSAX sax;
    if (!nlohmann::json::sax_parse(contents, &sax)) {
        return {};
    }
    return std::make_shared<hideout>(std::move(sax.proj_));
}

std::optional<std::string> export_hideout_json_format(hideout const &p) {
    using namespace std::string_view_literals;
    fmt::memory_buffer buf;

    buf.append("\xEF\xBB\xBF"sv);

    fmt::format_to(fmt::appender(buf), R"({{
  "version": 1,
  "language": "{}",
  "hideout_name": "{}",
  "hideout_hash": {},
)",
                   p.language_, p.hideout_name_, p.hideout_hash_);

    if (p.music_name_ && p.music_hash_) {
        fmt::format_to(fmt::appender(buf), R"(  "music_name": "{}",
  "music_hash": {},
)",
                       *p.music_name_, *p.music_hash_);
    }
    if (p.environment_) {
        fmt::format_to(fmt::appender(buf), R"(  "environment": {}",
)",
                       *p.environment_);
    }

    buf.append(R"(  "doodads": {
)"sv);

    size_t const obj_count = p.objects_.size();
    for (size_t obj_idx = 0; obj_idx < obj_count; ++obj_idx) {
        auto const &obj = p.objects_[obj_idx];
        uint8_t fv = (obj.flip_ ? 0x80 : 0) | obj.var_;
        bool is_last = obj_idx + 1 == obj_count;
        fmt::format_to(fmt::appender(buf), R"(    "{}": {{
      "hash": {},
      "x": {},
      "y": {},
      "r": {},
      "fv": {}
    }}{}
)",
                       obj.name_, obj.hash_, obj.x_, obj.y_, obj.rot_, fv, (is_last ? "" : ","));
    }

    buf.append("  }\n}"sv);

    return to_string(buf);
}

std::optional<hideout_object> hideout_object::from_json(nlohmann::json js) {
    // Verify schema
    bool valid_schema = js["name"].is_string() && js["hash"].is_number_unsigned() && js["x"].is_number_unsigned() &&
                        js["y"].is_number_unsigned() && js["r"].is_number_unsigned() && js["fv"].is_number_unsigned();
    if (!valid_schema) {
        return {};
    }

    auto fv = js["fv"].get<uint8_t>();

    // Build result
    return hideout_object{
        .name_ = js["name"],
        .hash_ = js["hash"],
        .x_ = js["x"],
        .y_ = js["y"],
        .rot_ = js["r"],
        .flip_ = !!(fv & 0x80u),
        .var_ = (uint8_t)(fv & 0x7Fu),
    };
}

nlohmann::json hideout_object::to_json() const {
    return {name_,
            {
                {
                    "hash",
                    hash_,
                },
                {
                    "x",
                    x_,
                },
                {
                    "y",
                    y_,
                },
                {
                    "r",
                    rot_,
                },
                {
                    "fv",
                    (flip_ ? 0x80u : 0u) | var_,
                },
            }};
}

} // namespace poe_hideout