import poe_hideout
import sys

with open(sys.argv[1], encoding='utf-8-sig') as fh:
    data = fh.read()
    ho = poe_hideout.open_hideout(data)

    print(ho)
    print(f'Language: {ho.language}')
    print(f'Hideout name: {ho.hideout_name}')
    print(f'Hideout hash: {ho.hideout_hash}')
    print(f'Music name: {ho.music_name}')
    print(f'Music hash: {ho.music_hash}')
    print(f'Objects ({len(ho.objects)}):')
    print(ho.objects[0])
    ho.objects[0].x += 100
    print(ho.objects[0])