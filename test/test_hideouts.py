import pathlib
import unittest

import poe_hideout

class TestHideoutIO(unittest.TestCase):

    def test_load(self):
        ref_path = pathlib.Path(__file__).parent / "data" / "VerdantGarden.hideout"
        with open(ref_path, 'r', encoding='utf-8-sig') as fh:
            ho = poe_hideout.open_hideout(fh)
            self.assertTrue(ho)
            for obj in ho.objects[:10]:
                print(obj)